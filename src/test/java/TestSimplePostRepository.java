import by.itstep.homework.entity.Post;
import by.itstep.homework.repository.SimplePostRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestSimplePostRepository {

    SimplePostRepository simplePostRepository = new SimplePostRepository();

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void getById_happyPath() {
        //GIVEN

        //WHEN
        Post post = simplePostRepository.getById(1L);

        //THEN
        Assert.assertNotNull(post);
    }


    @Test
    public void findAll_happyPath() {

    }

    @Test
    public void save_happyPath() {

    }

    @Test
    public void update_happyPath() {

    }

    @Test
    public void delete_happyPath() {


    }
}

package by.itstep.homework;

import by.itstep.homework.util.EntityManagerUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);

        EntityManagerUtils.getEntityManager();
    }

}

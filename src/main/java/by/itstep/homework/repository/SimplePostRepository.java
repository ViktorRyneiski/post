package by.itstep.homework.repository;

import by.itstep.homework.entity.Post;
import by.itstep.homework.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class SimplePostRepository implements PostRepository {

    @Override
    public Post getById(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        Post foundPost = em.find(Post.class, id);

        em.close();

        return foundPost;
    }

    @Override
    public List<Post> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Post> foundPosts = em.createQuery("FROM Post",Post.class)
                .getResultList();

        em.close();

        return foundPosts;
    }

    @Override
    public void save(final Post post) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(post);

        em.getTransaction().commit();

        em.close();
    }

    @Override
    public void update(final Post post) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        Post postToUpdate = em.find(Post.class, post.getId());

        postToUpdate.setDate(post.getDate());
        postToUpdate.setMessage(post.getMessage());
        postToUpdate.setName(post.getName());
        postToUpdate.setUserName(post.getUserName());

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void delete(final Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        Post postToDelete = em.find(Post.class, id);
        em.remove(postToDelete);

        em.getTransaction().commit();
        em.close();
    }
}

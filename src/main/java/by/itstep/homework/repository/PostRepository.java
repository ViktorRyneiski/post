package by.itstep.homework.repository;

import by.itstep.homework.entity.Post;

import java.util.List;

public interface PostRepository {

    Post getById(Long id);

    List<Post> findAll();

    void save(Post post);

    void update(Post post);

    void delete(Long id);

}

package by.itstep.homework.controller;

import by.itstep.homework.entity.Post;
import by.itstep.homework.repository.PostRepository;
import by.itstep.homework.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PostController {

    @Autowired
    private PostService postService;

    //    7. Создать контроллер который обрабатывает следующие запросы:
//    • @GetMapping(“/posts”) -> Открывает страницу со списком всех постов
    @GetMapping("/posts")
    public String showAllPosts(Model model) {
        List<Post> foundPosts = postService.findAll();
        model.addAttribute("posts", foundPosts);
        return "all-posts";
    }

    //    • @GetMapping(“/post/{id}”) -> Открывает конкретный пост по ID
    @ResponseBody
    @GetMapping(value = "/post/{id}")
    public Post getById(@PathVariable Long id, Model model) {
        Post foundPost = postService.getById(id);
        return foundPost;
//        model.addAttribute("post", foundPost);
//        return "single-post";
    }

    //    • @GetMapping(“/post/create-form”) -> Открывает страницу с формой создания
    @GetMapping("/post/create-form")
    public String createNewPost(Model model) {
        model.addAttribute("post", new Post());
        return "create-post";
    }

    //    • @PostMapping(“/post/create”) -> Принимает пост и сохраняет его в БД
    @PostMapping("/post/create")
    public String createNews(Post post) {
        postService.save(post);
        return "redirect:/posts";
    }




    @GetMapping("/update-post/{id}")
    public String openUpdatingPage(@PathVariable Long id, Model model) {
        Post foundPost = postService.getById(id);
        model.addAttribute("post", foundPost);

        return "update-post";
    }

    @PostMapping("/update-post/{id}")
    public String update(Post post) {
        postService.update(post);

        return "redirect:/posts";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        postService.delete(id);
        return "redirect:/posts";
    }
}

package by.itstep.homework.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Класс который позволит нам получать EntityManager'ов
// Им будет пользоваться каждый CRUD репозиторий
public class EntityManagerUtils {

    // Мы создает EntityManagerFactory только 1 раз при запуске приложения
    // этот класс позволит нам создавать много EntityManager'ов
    // При создании этой фабрики, он вычитывает persistence.xml
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY =
            Persistence.createEntityManagerFactory("post-itstep");

    // EntityManager'ы нужны нашим CRUD репозиториям! (Как connection'ы в JDBC)
    public static EntityManager getEntityManager() {
        return ENTITY_MANAGER_FACTORY.createEntityManager();
    }

}

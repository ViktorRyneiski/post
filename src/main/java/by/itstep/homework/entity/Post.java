package by.itstep.homework.entity;

import java.sql.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "message")
    private String message;

    @Column(name = "date")
    private Date date;

    @Column(name = "user_name")
    private String userName;

    public Post() {
    }

    public Post(final String name, final String message, final Date date, final String userName) {
        this.name = name;
        this.message = message;
        this.date = date;
        this.userName = userName;
    }

    public static Builder getBuilder(){
        return new Builder();
    }

    public static class Builder{
        private Post post = new Post();

        public Builder id(Long id){
            post.id = id;
            return this;
        }

        public Builder name(String name){
            post.name = name;
            return this;
        }

        public Builder message(String message){
            post.message = message;
            return this;
        }

        public Builder date(Date date){
            post.date = date;
            return this;
        }

        public Post build(){
            return post;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Post post = (Post) o;
        return Objects.equals(id, post.id) &&
                Objects.equals(name, post.name) &&
                Objects.equals(message, post.message) &&
                Objects.equals(date, post.date) &&
                Objects.equals(userName, post.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, message, date, userName);
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", date=" + date +
                ", userName='" + userName + '\'' +
                '}';
    }
}

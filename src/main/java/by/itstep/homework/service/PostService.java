package by.itstep.homework.service;

import by.itstep.homework.entity.Post;

import java.util.List;

public interface PostService {

    Post getById(Long id);

    List<Post> findAll();

    void save(Post user);

    void update(Post user);

    void delete(Long id);

}

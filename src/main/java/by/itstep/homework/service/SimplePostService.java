package by.itstep.homework.service;

import by.itstep.homework.entity.Post;
import by.itstep.homework.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimplePostService implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public Post getById(final Long id) {
        System.out.println("Service -> find by id: " + id);
        Post foundPost = postRepository.getById(id);
        System.out.println("Service -> found: " + foundPost);

        return foundPost;
    }

    @Override
    public List<Post> findAll() {
        System.out.println("Service -> findAll");
        List<Post> foundPosts = postRepository.findAll();
        System.out.println("Service: found " + foundPosts.size() + " posts...");

        return foundPosts;
    }

    @Override
    public void save(final Post post) {
        System.out.println("Service -> before save");
        postRepository.save(post);
        System.out.println("Service -> after save");

    }

    @Override
    public void update(final Post post) {
        System.out.println("Service -> before update");
        postRepository.update(post);
        System.out.println("Service -> after update");

    }

    @Override
    public void delete(final Long id) {
        System.out.println("Service -> before delete");
        postRepository.delete(id);
        System.out.println("Service -> after delete");

    }
}
